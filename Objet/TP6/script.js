let textAlertEmptyInput = document.getElementById("textAlertEmptyInput");
let textAlertAdd = document.getElementById("textAlertAdd");
let textAlertEmptyList = document.getElementById("textAlertEmptyList");
let textAlertProductDelete = document.getElementById("textAlertProductDelete");
let input = document.getElementById("input");
let buttonAddon2 = document.getElementById("button-addon2");
let liste = document.getElementById("liste");




input.addEventListener("keypress", function (event) {
    if (event.key === "Enter") {
        inputValidation();
    }
});

buttonAddon2.addEventListener("click", function (event) {
    inputValidation();
});




function inputValidation() {
    if (input.value === "") {
        textAlertEmptyInput.style.display = "block";
        textAlertAdd.style.display = "none";
        textAlertEmptyList.style.display = "none";
        textAlertProductDelete.style.display = "none";
    }
    else {
        textAlertAdd.style.display = "block";
        vider.style.display = "block";
        textAlertEmptyInput.style.display = "none";
        textAlertEmptyList.style.display = "none";
        textAlertProductDelete.style.display = "none";
        addProduct();
    }
}



function addProduct() {
    liste.innerHTML += `
    <span class="d-flex justify-content-between mx-4">
        ${input.value}
        <div class="d-flex gap-3">
            <i class="bi bi-pencil-square text-success" onclick="editPost(this)"></i>
            <i class="bi bi-trash3 text-danger" onclick="deletePost(this)"></i>
        </div>
    </span>`;
    input.value = "";
}



function deletePost(e) {
    e.parentElement.parentElement.remove();
    textAlertProductDelete.style.display = "block";
    textAlertAdd.style.display = "none";
    textAlertEmptyInput.style.display = "none";
    textAlertEmptyList.style.display = "none";
}

//Doesn't work, idk why
function editPost(e) {
    input.value = e.parentElement.previousElementSibling.innerHTML;
    e.parentElement.parentElement.remove();
    textAlertProductDelete.style.display = "none";
    textAlertAdd.style.display = "none";
    textAlertEmptyInput.style.display = "none";
    textAlertEmptyList.style.display = "none";
}