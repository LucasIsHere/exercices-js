class Voiture {
    constructor(marque, modele, vitesse) {
        this.marque = marque;
        this.modele = modele;
        this.vitesse = vitesse;
    }

    speed(nbAccel, nbTournants) {
        let vitesse;
        vitesse = this.vitesse + nbAccel * 10 - nbTournants * 5;
        return vitesse;
    }
}


let voiture1 = new Voiture("BMW", "Série 1", 80);
let voiture2 = new Voiture("Mercedes", "GLB", 100);

console.log(`Voiture 1, 3 accélérations : vitesse = ${voiture1.speed(3, 0)}`);
console.log(`Voiture 2, 2 accélérations, 2 tournants : vitesse = ${voiture2.speed(2, 2)}`);
