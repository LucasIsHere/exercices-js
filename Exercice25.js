//#region Exercice 25

var prompt = require("prompt-sync")();

let nb = parseInt(prompt("Entrer un nombre : ")),
    nbMax = nb;

for (let index = 1; index < 20; index++) {
    nb = parseInt(prompt("Entrer un nombre : "));
    if (nb > nbMax) {
        nbMax = nb;
    }
}

console.log("Le nombre le plus grand est " + nbMax);

//#endregion
