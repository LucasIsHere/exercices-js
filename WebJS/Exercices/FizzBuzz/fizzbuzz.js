var val = 0;

function augmentation() {
    val++;
    fizzBuzz();
}

function diminution() {
    val--;
    fizzBuzz();
}

function fizzBuzz() {
    if (val % 3 == 0 && val % 5 == 0) {
        let valeur = document.querySelector("#valeur");
        valeur.textContent = "FizzBuzz";
        valeur.style.fontWeight = "bolder";
        valeur.style.color = "red";
    }
    else if (val % 3 == 0) {
        let valeur = document.querySelector("#valeur");
        valeur.textContent = "Buzz";
        valeur.style.fontWeight = "bolder";
        valeur.style.color = "blue";
    }
    else if (val % 5 == 0) {
        let valeur = document.querySelector("#valeur");
        valeur.textContent = "Fizz";
        valeur.style.fontWeight = "bolder";
        valeur.style.color = "green";
    }
    else {
        valeur.textContent = val;
        
    }
}
