let aDeviner = Math.floor(Math.random() * 50),
    compteur = 0, proposition;

document.querySelector("#reponse").textContent = aDeviner;

function proposer() {
    proposition = document.querySelector("#proposition").value;
    switch (true) {
        case proposition > aDeviner:
            document.querySelector("#verifProp").textContent = "Plus grand que le nombre mystère !";
            compteur++;
            document.querySelector("#compteur").textContent = `Nombre de coups : ${compteur}`;
            break;
        case proposition < aDeviner:
            document.querySelector("#verifProp").textContent = "Plus petit que le nombre mystère !";
            compteur++;
            document.querySelector("#compteur").textContent = `Nombre de coups : ${compteur}`;
            break;
        case proposition == aDeviner:
            document.querySelector("#verifProp").textContent = "Bravo, vous avez trouvé le nombre mystère !";
            document.querySelector("#compteur").textContent = `Nombre de coups : ${compteur}`;
        default:
            break;
    }
}

function rejouer() {
    window.location.reload();
}
