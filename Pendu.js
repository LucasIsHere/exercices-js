var prompt = require("prompt-sync")();
let mots = ["indien", "mort", "roseau", "victoire", "alcool", "byciclette", "colon", "bombe", "laboratoire", "gondole",
    "flocon", "skateboard", "hier", "fragment", "potable", "doberman", "aspirer", "chef", "destructeur", "des"],
    motADeviner, masque = [], lettre, nbTentatives = 5, changement, comparaison;


//Choix mot aléatoire parmis la liste de mots prédéfinis

motADeviner = mots[Math.floor(Math.random() * 19)];
// console.log(`Le mot à deviner est ${motADeviner}\n`);
motADeviner = Array.from(motADeviner);


//Génère un masque de même longueur que notre mot

for (let i = 0; i < motADeviner.length; i++) {
    masque[i] = '_';
}


//Fonction qui demande à l'utilisateur d'entrer une lettre et vérifie si elle est dans notre mot

function proposition() {
    lettre = prompt(`Quelle lettre proposez-vous ? `);
    for (let index = 0; index < motADeviner.length; index++) {
        if (lettre === motADeviner[index]) {
            masque[index] = lettre;
            changement = true;
        }
    }
}

const comparaisonTab = function (a1, a2) {
    var i = a1.length;
    if (i != a2.length) {
        return false;
    }
    while (i--) {
        if (a1[i] !== a2[i]) return false;
    }
    return true;
};

console.log(`Essayez de deviner le mot : ${masque.toString().replace(/,/g, '')}\nVous avez ${nbTentatives} tentatives.\n`);

do {
    proposition();
    if (changement == true) {
        console.log(`La lettre ${lettre} est dans le mot à deviner !\n`);
        console.log(`${masque.toString().replace(/,/g, '')}\n`);
        changement = false;
        comparaison = comparaisonTab(masque, motADeviner)

        if (comparaison == true) {
            nbTentatives = 0;
            console.log(`Bravo, vous avez trouver le mot caché : ${motADeviner.toString().replace(/,/g, '')}`)
        }
    }
    else {
        nbTentatives--;
        switch (true) {
            case nbTentatives == 0:
                console.log(`Perdu ! le mot à deviner était : ${motADeviner.toString().replace(/,/g, '')}\nVous avez été pendu !!`);
                break;
            case nbTentatives == 1:
                console.log(`Eh non, ${lettre} n'est pas dans le mot.\nIl ne vous reste plus qu'une tentative !\n`);
                console.log(`${masque.toString().replace(/,/g, '')}\n`);
                break;

            default:
                console.log(`Eh non, ${lettre} n'est pas dans le mot.\nIl vous reste ${nbTentatives} tentatives.\n`);
                console.log(`${masque.toString().replace(/,/g, '')}\n`);
                break;
        }
    }
} while (nbTentatives != 0);
