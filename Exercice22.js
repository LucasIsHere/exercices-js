//#region Exercice 22

var prompt = require("prompt-sync")();

let nombre;

do {
    nombre = prompt("Donnez moi un nombre entre 10 et 20 : ");
    if (nombre > 20) {
        console.log("Plus petit !");
    }
    else if (nombre < 10) {
        console.log("Plus grand !");
    }
    else {
        console.log("Merci !");
    }
} while (nombre < 10 || nombre > 20);

//#endregion
