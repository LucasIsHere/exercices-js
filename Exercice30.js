//#region Exercice 30

let tab = ['D', 'É', 'C', 'A', 'L', 'A', 'G', 'E'];

console.log(`Tableau de départ : ${tab}`);

let temp = tab[0];

tab.shift();
tab.push(temp);

console.log(`Tableau décalé à gauche : ${tab}`);

//#endregion
