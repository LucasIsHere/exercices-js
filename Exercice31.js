//#region Exercice 31
//Je n'ai pas compris la consigne, j'ai fait un tri à bulle

var prompt = require("prompt-sync")();

let tab = [], changement = 1;

//Saisie tableau
for (let index = 0; index < 5; index++) {
    tab[index] = parseInt(prompt(`Valeur ${index + 1} : `));
}

console.log(`Tableau de base : ${tab}`);

//Tri tableau
while (changement != 0) {
    for (let index = 0; index < tab.length - 1; index++) {
        if (tab[index] > tab[index + 1]) {
            let temp = tab[index];
            tab[index] = tab[index + 1];
            tab[index + 1] = temp;
            changement = changement + 1;
        }
    }
    changement = changement - 1;
}

console.log(`Tableau trié : ${tab}`);

//#endregion
