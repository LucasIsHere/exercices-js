//#region Exercice 28

function secondes(heures, minutes, secondes) {
    let h = heures * 3600,
        m = minutes * 60,
        total = h + m + secondes;
    return total;
}

var prompt = require("prompt-sync")();

let heures = parseInt(prompt(`Nombre d'heures : `)),
    minutes = parseInt(prompt(`Nombre de minutes : `)),
    nbsecondes = parseInt(prompt(`Nombre de secondes : `));

console.log(`${heures} heures, ${minutes} minutes et ${nbsecondes} secondes équivaut à ${secondes(heures, minutes, nbsecondes)} secondes`);

//#endregion
