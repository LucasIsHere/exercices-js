//#region Exercice 3

var prompt = require("prompt-sync")();

console.log("La valeur de π est : " + Math.PI);

let rayon = prompt("Rayon du cercle (en cm) : "),
    diametre = rayon * 2,
    perimetre = Math.PI * (rayon * 2),
    aire = Math.PI * (rayon ** 2);

console.log("Diamètre = " + diametre + "cm");
console.log("Périmètre = " + perimetre + "cm");
console.log("Aire = " + aire + "cm²");
console.log("Périmètre (arrondi) = " + Math.round(perimetre) + "cm");
console.log("Aire (arrondi) = " + Math.round(aire) + "cm²");
//#endregion
