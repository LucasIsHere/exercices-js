//#region Exercice 17

var prompt = require("prompt-sync")();

let netImposableFoyer = prompt("Montant net imposable : "),
    nbreAdultes = prompt("Nombre d'adultes : "),
    nbreEnfants = prompt("Nombre d'enfants : "),
    nbreParts, netImposable, impots;

if (nbreEnfants >= 3) {
    nbreParts = nbreAdultes * 1 + 2 * 0.5 + (nbreEnfants - 2) * 1;
}
else {
    nbreParts = nbreAdultes * 1 + nbreEnfants * 0.5;
}

netImposable = (netImposableFoyer / nbreParts);

if (netImposable > 10084 && netImposable <= 25710) {
    impots = (netImposable - 10084) * 0.11 * nbreParts;
    console.log("Le montant de l'impôt sur le revenu pour un foyer composé de " + nbreAdultes + " adulte(s) et de "
        + nbreEnfants + " enfant(s), avec un revenu fiscal de " + netImposableFoyer + "€ s'élève à " + Math.round(impots) + "€");
}
else if (netImposable > 25710 && netImposable <= 73516) {
    impots = (netImposable - 25710) * 0.3 * nbreParts;
    console.log("Le montant de l'impôt sur le revenu pour un foyer composé de " + nbreAdultes + " adulte(s) et de "
        + nbreEnfants + " enfant(s), avec un revenu fiscal de " + netImposableFoyer + "€ s'élève à " + Math.round(impots) + "€");
}
else if (netImposable > 73516 && netImposable <= 158122) {
    impots = (netImposable - 73516) * 0.41 * nbreParts;
    console.log("Le montant de l'impôt sur le revenu pour un foyer composé de " + nbreAdultes + " adulte(s) et de "
        + nbreEnfants + " enfant(s), avec un revenu fiscal de " + netImposableFoyer + "€ s'élève à " + Math.round(impots) + "€");
}
else if (netImposable > 158122) {
    impots = (netImposable - 158122) * 0.45 * nbreParts;
    console.log("Le montant de l'impôt sur le revenu pour un foyer composé de " + nbreAdultes + " adulte(s) et de "
        + nbreEnfants + " enfant(s), avec un revenu fiscal de " + netImposableFoyer + "€ s'élève à " + Math.round(impots) + "€");
}
else {
    console.log("Vous n'êtes pas imposable.")
}

//#endregion
