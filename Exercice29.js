//#region Exercice 29

var prompt = require("prompt-sync")();

let notes = [], somme = 0, moyenne;

for (let index = 0; index < 5; index++) {
    notes[index] = parseInt(prompt(`Note élève ${index+1} : `));
    somme = somme + notes[index];
}

moyenne = somme / notes.length;

console.log(notes);

console.log(`Moyenne = ${moyenne}`);

console.log(`Note la plus élevée : ${Math.max(...notes)}`);
console.log(`Note la plus basse : ${Math.min(...notes)}`);

//#endregion
