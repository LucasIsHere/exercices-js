//#region Exercice 24

var prompt = require("prompt-sync")();

let prix, somme = 0, paiement, aRendre, reste, nb10, nb5, n;

do {
    prix = parseInt(prompt("Prix produit (0 si listing terminé) : "));
    somme = somme + prix;
} while (prix > 0 || prix < 0);

console.log("Montant dû : " + somme);

do {
    paiement = parseInt(prompt("Montant donné ? "));
    if (paiement < somme) {
        console.log("Vous n'avez pas donné assez.")
    }
} while (paiement < somme);

aRendre = paiement - somme;
console.log("Montant à rendre : " + aRendre);

//Ne fonctionne pas
// if (aRendre % 10 >= 0) {
//     reste = aRendre % 10;
//     aRendre = aRendre - reste;
//     nb10 = aRendre / 10;
//     aRendre = aRendre - nb10 * 10;
// }
// else if (aRendre % 5 >= 0) {
//     reste = aRendre % 5;
//     aRendre = aRendre - reste;
//     nb5 = aRendre / 5;
//     aRendre = aRendre - nb5 * 5;
// }

// if (nb10 > 0) {
//     for (let index = 1; index <= nb10; index++) {
//         console.log("10 euros");
//     }
// }
// else if (nb5 > 0) {
//     for (let index = 0; index <= nb5; index++) {
//         console.log("5 euros");
//     }
// }

// for (let index = 0; index < aRendre; index++) {
//     console.log("1 euro");
// }

//#endregion
