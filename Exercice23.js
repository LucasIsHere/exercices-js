//#region Exercice 23

var prompt = require("prompt-sync")();

let nombre = prompt("Quelle table souhaitez-vous visualiser ? ");
console.log("Table de " + nombre + " :");

for (let index = 1; index <= 10; index++) {
    console.log(nombre + " x " + index + " = " + nombre*index);
}

//#endregion
