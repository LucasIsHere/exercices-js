//#region Exercice 13

var prompt = require("prompt-sync")();

let age = prompt("Saisissez l'âge de votre enfant : ");

if (age <= 6 && age >= 3) {
    console.log("Catégorie Baby");
}
else if (age >= 7 && age <= 8) {
    console.log("Catégorie Poussin");
}
else if (age >= 9 && age <= 10) {
    console.log("Catégorie Pupille");
}
else if (age >= 11 && age <= 12) {
    console.log("Catégorie Minime");
}
else if (age >= 13 && age < 18) {
    console.log("Catégorie Cadet");
}
else if (age >= 18) {
    console.log("Ce n'est plus un enfant, RÉVEILLEZ-VOUS");
}
else if (age < 3) {
    console.log("Votre enfant est trop jeune pour pratiquer, CONCENTREZ-VOUS")
}
//#endregion
