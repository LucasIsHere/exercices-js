//#region Exercice 4

var prompt = require("prompt-sync")();

function checkPalindrome(string) {
    const stringToArray = string.split('');
    const reverseArrayValues = stringToArray.reverse();
    const arrayToString = reverseArrayValues.join('');
    console.log("Le mot inversé : " + arrayToString);
    if (string == arrayToString) {
        console.log("Le mot " + string + " est un palindrome.");
    }
    else {
        console.log("Le mot " + string + " n'est pas un palindrome.");
    }
}

const mot = prompt('Enter a string: ');

checkPalindrome(mot);
//#endregion
