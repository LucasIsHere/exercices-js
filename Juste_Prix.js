//#region Le juste prix

var prompt = require("prompt-sync")();

function getRandomIntInclusive(min, max) {
    min = Math.ceil(min);
    max = Math.floor(max);
    return Math.floor(Math.random() * (max - min + 1)) + min;
}

let aDeviner = getRandomIntInclusive(0, 1000), proposition, compteur = 0;

//console.log(`Le prix à deviner est ${aDeviner}`);

do {
    proposition = parseInt(prompt(`Quelle est votre proposition ? `));
    compteur++;

    if (proposition > aDeviner) {
        console.log(`C'est moins.`);
    }
    else if (proposition < aDeviner) {
        console.log(`C'est plus.`);
    }
} while (proposition != aDeviner);

console.log(`Bravo, vous avez trouvé le juste prix !
Votre nombre de tentatives s'élève à ${compteur}`);

//#endregion
