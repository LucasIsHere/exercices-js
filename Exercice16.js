//#region Exercice 16

var prompt = require("prompt-sync")();

let age = prompt("Quel est votre âge ? "),
    anciennete = prompt("Combien d'années d'ancienneté avez-vous ? "),
    lastSalaire = prompt("Montant de votre dernier salaire : "),
    indemnite;

if (anciennete >= 1 && anciennete <= 10) {
    if (age >= 46 && age <= 49) {
        indemnite = 2 * lastSalaire + (lastSalaire / 2) * anciennete;
    }
    else if (age >= 50) {
        indemnite = 5 * lastSalaire + (lastSalaire / 2) * anciennete;
    }
    else {
        indemnite = (lastSalaire / 2) * anciennete;
    }
    console.log("Le montant de l'indemnité pour un salarié de " + age + " ans et avec "
    + anciennete + " années d'ancienneté s'élève à " + Math.round(indemnite) + "€");
}
else if (anciennete > 10) {
    if (age >= 46 && age <= 49) {
        indemnite = 2 * lastSalaire + lastSalaire * anciennete;
    }
    else if (age >= 50) {
        indemnite = 5 * lastSalaire + lastSalaire * anciennete;
    }
    else {
        indemnite = lastSalaire * anciennete;
    }
    console.log("Le montant de l'indemnité pour un salarié de " + age + " ans et avec "
    + anciennete + " années d'ancienneté s'élève à " + Math.round(indemnite) + "€");
}
else {
    console.log("Vous n'avez pas assez d'ancienneté pour prétendre à une indemnité de licenciement.");
}

//#endregion
