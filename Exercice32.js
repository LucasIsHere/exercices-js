//#region Exercice 32

var prompt = require("prompt-sync")();

let tab1 = [], tab2 = [];

for (let index = 0; index < 5; index++) {
    tab1[index] = parseInt(prompt(`Valeur ${index + 1} : `));
}

console.log(`Tableau de base : ${tab1}`);

for (let i = 0; i < tab1.length; i++) {
    tab2[i] = tab1[tab1.length-1-i];
}

console.log(`Tableau inversé : ${tab2}`);

//#endregion
